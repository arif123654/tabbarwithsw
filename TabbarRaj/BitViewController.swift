//
//  BitViewController.swift
//  TabbarRaj
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class BitViewController: UIViewController {
    //MARK:- Utilities
  let vc = AVPlayerViewController()
//    var avPlayer:AVPlayer?
    @IBOutlet weak var viewTableView: UITableView!
  // var N = [String]()
    var s = [ "https://www.videvo.net/videvo_files/converted/2016_06/videos/160323_15_PokerTracking1_1080p.mov45606.mp4", "https://www.videvo.net/videvo_files/converted/2015_02/videos/Woodpile_01_Videvo.mov56517.mp4", "https://www.videvo.net/videvo_files/converted/2016_01/videos/Forest_15_2_Videvo.mov92730.mp4", "https://www.videvo.net/videvo_files/converted/2016_01/videos/Forest_15_2_Videvo.mov92730.mp4", "https://www.videvo.net/videvo_files/converted/2015_08/videos/Evening_landing.mp412257.mp4" ]
    
    
    //MARK:- Init
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTableView.delegate = self
        viewTableView.dataSource = self
        viewTableView.rowHeight = 350
       
    }
    //MARK:- Thubmnail Generator
          func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
              let asset = AVAsset(url: URL(string: url)!)
              let assetImgGenerate = AVAssetImageGenerator(asset: asset)
              assetImgGenerate.appliesPreferredTrackTransform = true
              assetImgGenerate.maximumSize = CGSize(width: 240, height: 128)
            let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
              do {
                  let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                  let thumbnail = UIImage(cgImage: img)
                  return thumbnail
              } catch {
                print(error.localizedDescription)
                return nil
              }
          }
    func generateThumbnail(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: Int64(1.0), timescale: 600), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
}





//MARK:- TableViewController
extension BitViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return s.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewTableView.dequeueReusableCell(withIdentifier: "cell") as! BiTTableViewCell
        let path = URL(string: s[indexPath.row])
        cell.thumbnailImageView.image = generateThumbnail(path: path! )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let path = URL(string: s[indexPath.row])
        let player = AVPlayer(url: path!)
               vc.player = player
             present(vc, animated: true){
                self.vc.player?.play()
                self.vc.entersFullScreenWhenPlaybackBegins = true
                self.vc.exitsFullScreenWhenPlaybackEnds = true
        }
    }
    
}
  
